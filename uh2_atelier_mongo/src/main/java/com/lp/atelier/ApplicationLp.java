package com.lp.atelier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationLp {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationLp.class, args);
	}

}
