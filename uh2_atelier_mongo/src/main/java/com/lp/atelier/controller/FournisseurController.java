package com.lp.atelier.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lp.atelier.model.Commande;
import com.lp.atelier.model.Fournisseur;
import com.lp.atelier.repository.FournisseurRepository;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class FournisseurController {

  @Autowired
  FournisseurRepository fournisseurRepository;

  @GetMapping("/fournisseurs")
  public ResponseEntity<List<Fournisseur>> getAllFournisseurs() {
    try {
      List<Fournisseur> fournisseurs = fournisseurRepository.findAll();

      if (fournisseurs.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(fournisseurs, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/fournisseurs/{id}")
  public ResponseEntity<Fournisseur> getFournisseurById(@PathVariable("id") String id) {
    Optional<Fournisseur> fournisseurData = fournisseurRepository.findById(id);

    if (fournisseurData.isPresent()) {
      return new ResponseEntity<>(fournisseurData.get(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/fournisseurs")
  public ResponseEntity<Fournisseur> createFournisseur(@RequestBody Fournisseur fournisseur) {
    try {
      Fournisseur _fournisseur = new Fournisseur(fournisseur.getNomFournisseur(), fournisseur.getTelephone(), fournisseur.getEmail(), fournisseur.getAdresse());
      List<Commande> listCommande = new ArrayList<Commande>();     
      for(Commande commande : fournisseur.getCommandes()) {
    	  if(commande.getDateExpiration().after(new Date())) {
        	  listCommande.add(commande);
    	  }
      }
      _fournisseur.setCommandes(listCommande); // Choix 1 : Traitement a applique sur chaque commande
      //_fournisseur.setCommandes(fournisseur.getCommandes()); // Choix 2: Traitement direct
      
      fournisseurRepository.save(_fournisseur);
      
      return new ResponseEntity<>(_fournisseur, HttpStatus.CREATED);
    } catch (Exception e) {
    	System.out.println(e.getMessage());
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/fournisseurs/{id}")
  public ResponseEntity<Fournisseur> updateFournisseur(@PathVariable("id") String id, @RequestBody Fournisseur fournisseur) {
    Optional<Fournisseur> fournisseurData = fournisseurRepository.findById(id);

    if (fournisseurData.isPresent()) {
      Fournisseur _fournisseur = fournisseurData.get();
      _fournisseur.setNomFournisseur(fournisseur.getNomFournisseur());
      _fournisseur.setTelephone(fournisseur.getTelephone());
      _fournisseur.setEmail(fournisseur.getEmail());
      _fournisseur.setAdresse(fournisseur.getAdresse());
      return new ResponseEntity<>(fournisseurRepository.save(_fournisseur), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/fournisseurs/{id}")
  public ResponseEntity<HttpStatus> deleteFournisseur(@PathVariable("id") String id) {
    try {
      fournisseurRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @DeleteMapping("/fournisseurs")
  public ResponseEntity<HttpStatus> deleteAllFournisseurs() {
    try {
      fournisseurRepository.deleteAll();
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
