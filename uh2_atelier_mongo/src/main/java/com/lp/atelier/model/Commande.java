package com.lp.atelier.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Commande {
	
	private String reference;
	
	@JsonFormat(pattern="dd-MM-yyyy")
	private Date dateExpiration;
	
	private String designation;
	
	private String quantite;

	public Commande() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Commande(String reference, Date dateExpiration, String designation, String quantite) {
		super();
		this.reference = reference;
		this.dateExpiration = dateExpiration;
		this.designation = designation;
		this.quantite = quantite;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Date getDateExpiration() {
		return dateExpiration;
	}

	public void setDateExpiration(Date dateExpiration) {
		this.dateExpiration = dateExpiration;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getQuantite() {
		return quantite;
	}

	public void setQuantite(String quantite) {
		this.quantite = quantite;
	}

	

}
