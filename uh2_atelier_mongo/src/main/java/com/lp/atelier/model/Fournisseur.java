package com.lp.atelier.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection= "Suppliers")
public class Fournisseur {

	@Id
	private String id;

	private String nomFournisseur;
	private String telephone;
	private String email;
	private String adresse;
	
	@DBRef
	private List<Commande> commandes;
	
	
	public Fournisseur() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Fournisseur(String id, String nomFournisseur, String telephone, String email, String adresse) {
		super();
		this.id = id;
		this.nomFournisseur = nomFournisseur;
		this.telephone = telephone;
		this.email = email;
		this.adresse = adresse;
	}
	
	public Fournisseur(String nomFournisseur, String telephone, String email, String adresse) {
		super();
		this.nomFournisseur = nomFournisseur;
		this.telephone = telephone;
		this.email = email;
		this.adresse = adresse;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNomFournisseur() {
		return nomFournisseur;
	}


	public void setNomFournisseur(String nomFournisseur) {
		this.nomFournisseur = nomFournisseur;
	}


	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}
	
}
