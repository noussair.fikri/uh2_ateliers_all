package com.lp.atelier.repository;


import org.springframework.data.mongodb.repository.MongoRepository;

import com.lp.atelier.model.Fournisseur;

public interface FournisseurRepository extends MongoRepository<Fournisseur, String> {

}
