package com.uh2.ateliers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UH2Application {

	public static void main(String[] args) {
		
//		System.out.println("Argument 1"+args[0]);	
//		System.out.println("Argument 2"+args[1]);	
		SpringApplication.run(UH2Application.class, args);

	}

}
