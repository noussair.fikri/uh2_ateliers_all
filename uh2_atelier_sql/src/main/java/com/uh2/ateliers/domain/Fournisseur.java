package com.uh2.ateliers.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
//@Table(name="SUPPLIER")
public class Fournisseur {
	
	// SELECT * FROM SUPPLIER => SQL
	// SELECT * FROM Fournisseur => HQL
	// CREATE TABLE SUPPLIER ( id BIGINT PRIMARY_KEY AUTOINCREMENT, nomFournisseur VARCHAR(255), .... ) 
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String nomFournisseur;
	private String telephone;
	private String email;
	private String adresse;
	
	@OneToMany(mappedBy="fournisseur", 
			cascade = CascadeType.ALL, 
			fetch = FetchType.LAZY)
	private List<Commande> commandes;
	
	
	public Fournisseur() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Fournisseur(Long id, String nomFournisseur, String telephone, String email, String adresse) {
		super();
		this.id = id;
		this.nomFournisseur = nomFournisseur;
		this.telephone = telephone;
		this.email = email;
		this.adresse = adresse;
	}
	
	public Fournisseur(String nomFournisseur, String telephone, String email, String adresse) {
		super();
		this.nomFournisseur = nomFournisseur;
		this.telephone = telephone;
		this.email = email;
		this.adresse = adresse;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomFournisseur() {
		return nomFournisseur;
	}


	public void setNomFournisseur(String nomFournisseur) {
		this.nomFournisseur = nomFournisseur;
	}


	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}
	
}
