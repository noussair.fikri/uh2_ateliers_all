package com.uh2.ateliers.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.uh2.ateliers.domain.Fournisseur;

public class CommandeDto {


	private Long id;

	private Fournisseur fournisseur;
	
	private String reference;
	
	@JsonFormat(pattern="dd-MM-yyyy")
	private Date dateExpiration;
	
	private String designation;
	
	private String quantite;

	
	public CommandeDto() {
		super();
		// TODO Auto-generated constructor stub
	}


	public CommandeDto(Long id, Fournisseur fournisseur, String reference, Date dateExpiration, String designation,
			String quantite) {
		super();
		this.id = id;
		this.fournisseur = fournisseur;
		this.reference = reference;
		this.dateExpiration = dateExpiration;
		this.designation = designation;
		this.quantite = quantite;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Fournisseur getFournisseur() {
		return fournisseur;
	}


	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}


	public String getReference() {
		return reference;
	}


	public void setReference(String reference) {
		this.reference = reference;
	}


	public Date getDateExpiration() {
		return dateExpiration;
	}


	public void setDateExpiration(Date dateExpiration) {
		this.dateExpiration = dateExpiration;
	}


	public String getDesignation() {
		return designation;
	}


	public void setDesignation(String designation) {
		this.designation = designation;
	}


	public String getQuantite() {
		return quantite;
	}


	public void setQuantite(String quantite) {
		this.quantite = quantite;
	}


}
