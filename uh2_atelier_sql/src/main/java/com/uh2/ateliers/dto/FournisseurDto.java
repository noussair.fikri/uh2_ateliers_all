package com.uh2.ateliers.dto;

import java.util.List;

import com.uh2.ateliers.domain.Commande;

public class FournisseurDto {
	
	private Long id;

	private String nomFournisseur;
	private String telephone;
	private String email;
	private String adresse;
	private List<CommandeDto> commandes;
	
	
	public FournisseurDto() {
		super();
		// TODO Auto-generated constructor stub
	}


	public FournisseurDto(Long id, String nomFournisseur, String telephone, String email, String adresse) {
		super();
		this.id = id;
		this.nomFournisseur = nomFournisseur;
		this.telephone = telephone;
		this.email = email;
		this.adresse = adresse;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNomFournisseur() {
		return nomFournisseur;
	}


	public void setNomFournisseur(String nomFournisseur) {
		this.nomFournisseur = nomFournisseur;
	}


	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public List<CommandeDto> getCommandes() {
		return commandes;
	}


	public void setCommandes(List<CommandeDto> commandes) {
		this.commandes = commandes;
	}
	
}
