package com.uh2.ateliers.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uh2.ateliers.domain.Commande;
import com.uh2.ateliers.domain.Fournisseur;
import com.uh2.ateliers.dto.CommandeDto;
import com.uh2.ateliers.dto.FournisseurDto;
import com.uh2.ateliers.repository.CommandeRepository;
import com.uh2.ateliers.repository.FournisseurRepository;

@Service
public class FournisseurServiceImpl implements FournisseurService {
	
	@Autowired
	FournisseurRepository fournisseurRepository;

	@Override
	public Fournisseur save(FournisseurDto fournisseurDto) {
		
		if(!fournisseurRepository.findByNomFournisseur(fournisseurDto.getNomFournisseur()).isEmpty()) {
			return null;
		}
		
		Fournisseur fournisseur = new Fournisseur();
		fournisseur.setNomFournisseur(fournisseurDto.getNomFournisseur());
		
		if(fournisseurDto.getTelephone().startsWith("06-")) {
			fournisseur.setTelephone(fournisseurDto.getTelephone().replaceAll("06-", "+212-6-"));
		}
		else {
			fournisseur.setTelephone(fournisseurDto.getTelephone());
		}

		fournisseur.setEmail(fournisseurDto.getEmail());
		fournisseur.setAdresse(fournisseurDto.getAdresse());
		
		//fournisseur.setCommandes(fournisseurDto.getCommandes());
		
		fournisseurRepository.save(fournisseur);
		
		List<Commande> listCommandes = new ArrayList<Commande>();
		
		for(CommandeDto commandeDto : fournisseurDto.getCommandes()) {
			Commande commandeTmp = new Commande();
			commandeTmp.setId(commandeDto.getId());
			commandeTmp.setFournisseur(fournisseur);
			commandeTmp.setReference(commandeDto.getReference());
			commandeTmp.setDateExpiration(commandeDto.getDateExpiration());
			commandeTmp.setDesignation(commandeDto.getDesignation());
			commandeTmp.setQuantite(commandeDto.getQuantite());
			// VERIFICATION COMMANDE
			listCommandes.add(commandeTmp);
		} 
		
		fournisseur.setCommandes(listCommandes);
		
		return fournisseurRepository.save(fournisseur); // => fournisseur avec ID generee;
	}

	@Override
	public Fournisseur update(FournisseurDto fournisseurDto) {
		
		if(!fournisseurRepository.existsById(fournisseurDto.getId())) { 
			return null;
		}
		
		Fournisseur fournisseur = new Fournisseur();
		fournisseur.setId(fournisseurDto.getId());
		fournisseur.setNomFournisseur(fournisseurDto.getNomFournisseur());
		
		if(fournisseurDto.getTelephone().startsWith("06-")) {
			fournisseur.setTelephone(fournisseurDto.getTelephone().replaceAll("06-", "+212-6-"));
		}
		else {
			fournisseur.setTelephone(fournisseurDto.getTelephone());
		}

		fournisseur.setEmail(fournisseurDto.getEmail());
		fournisseur.setAdresse(fournisseurDto.getAdresse());
		
		List<Commande> listCommandes = new ArrayList<Commande>();
		
		for(CommandeDto commandeDto : fournisseurDto.getCommandes()) {
			Commande commandeTmp = new Commande();
			commandeTmp.setFournisseur(fournisseur);
			commandeTmp.setId(commandeDto.getId());
			commandeTmp.setFournisseur(fournisseur);
			commandeTmp.setReference(commandeDto.getReference());
			commandeTmp.setDateExpiration(commandeDto.getDateExpiration());
			commandeTmp.setDesignation(commandeDto.getDesignation());
			commandeTmp.setQuantite(commandeDto.getQuantite());
			// VERIFICATION COMMANDE
			listCommandes.add(commandeTmp);
		} 
		
		fournisseur.setCommandes(listCommandes);
		
		return fournisseurRepository.save(fournisseur);
	}

	@Override
	public List<Fournisseur> findAll() {
		return fournisseurRepository.findAll();
	}

	@Override
	public void remove(long id) {
		if(fournisseurRepository.existsById(id)) { // Optional<Fournisseur> / get() => Fournisseur
			fournisseurRepository.delete(fournisseurRepository.findById(id).get());
		}
	}

	@Override
	public Optional<List<Fournisseur>> findByNomFournisseur(String nomFournisseur) {
		return fournisseurRepository.findByNomFournisseur(nomFournisseur);
	}

	@Override
	public Optional<Fournisseur> findById(long id) {
		return fournisseurRepository.findById(id);
	}

}
