package com.uh2.ateliers.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uh2.ateliers.domain.Commande;
import com.uh2.ateliers.domain.Fournisseur;

@Repository
public interface CommandeRepository extends JpaRepository<Commande,Long> {

	Commande findByReference(String reference);
	List<Commande> findByFournisseur(Fournisseur fournisseur);

}
