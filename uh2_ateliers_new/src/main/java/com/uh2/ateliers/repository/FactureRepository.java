package com.uh2.ateliers.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uh2.ateliers.domain.Facture;

@Repository
public interface FactureRepository extends JpaRepository<Facture,Long> {

	Facture findByReference(String reference);
}
