package com.uh2.ateliers.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uh2.ateliers.domain.Fournisseur;

@Repository
public interface FournisseurRepository extends JpaRepository<Fournisseur,Long> {
		// fournisseurRepository - getFournisseurRepository / SQL / HQL
	
	Optional<List<Fournisseur>> findByNomFournisseur(String nomFournisseur);
	// object InstanceOf List => isempty => liste est vide ou pas
	// object InstanceOf String => isempty => chaine est vide  ou pas
	// object InstanceOf Fournisseur => isPresent => null ou pas

	// SELECT * FROM SUPPLIER WHERE nom_fournisseur = ''
	// SELECT * FROM Fournisseur WHERE nomFournisseur = ''
	
	
}
