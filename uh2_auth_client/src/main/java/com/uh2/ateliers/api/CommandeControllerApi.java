package com.uh2.ateliers.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uh2.ateliers.domain.AuthBean;
import com.uh2.ateliers.domain.Client;
import com.uh2.ateliers.domain.Commande;
import com.uh2.ateliers.domain.Fournisseur;
import com.uh2.ateliers.dto.CommandeDto;
import com.uh2.ateliers.dto.FournisseurDto;
import com.uh2.ateliers.service.ClientService;
import com.uh2.ateliers.service.CommandeService;
import com.uh2.ateliers.service.FournisseurService;

@RestController
@RequestMapping("/public")
public class CommandeControllerApi {

	@Autowired
	CommandeService commandeService;
	
	@PostMapping("/addCommande")
	public ResponseEntity<Commande> addCommande(@RequestBody CommandeDto commandeDto) {
		Commande commande = commandeService.save(commandeDto);
		if(commande==null) return ResponseEntity.badRequest().body(null);
		else return ResponseEntity.ok().body(commande);
	}
	
	@DeleteMapping("/removeCommande/{id}")
	public ResponseEntity<?> exampleRemoveSupplier(@PathVariable Long id) { 
		commandeService.remove(id);
		return ResponseEntity.ok().body(null);		
	}
}
