package com.uh2.ateliers.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uh2.ateliers.domain.AuthBean;
import com.uh2.ateliers.domain.Client;
import com.uh2.ateliers.domain.Fournisseur;
import com.uh2.ateliers.dto.FournisseurDto;
import com.uh2.ateliers.service.ClientService;
import com.uh2.ateliers.service.FournisseurService;

@RestController
@RequestMapping("/public")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FournisseurControllerApi {

	@Autowired
	FournisseurService fournisseurService;
	
	@PostMapping("/addSupplier")
	public ResponseEntity<Fournisseur> exampleAddSupplier(@RequestBody FournisseurDto fournisseurDto) {
		Fournisseur f = fournisseurService.save(fournisseurDto);
		if(f==null) return ResponseEntity.badRequest().body(null);
		else return ResponseEntity.ok().body(f);
	}
	
	@PutMapping("/updateSupplier")
	public ResponseEntity<Fournisseur> exampleUpdateSupplier(@RequestBody FournisseurDto fournisseurDto) {
		Fournisseur f = fournisseurService.update(fournisseurDto);
		return ResponseEntity.ok().body(f);
	}
	
	@GetMapping("/getSuppliers")
	public ResponseEntity<List<Fournisseur>> exampleGetSuppliers() { 
		
		return ResponseEntity.ok().body(fournisseurService.findAll());	
	}
	
	@GetMapping("/getSupplier/{id}")
	public ResponseEntity<Fournisseur> exampleGetSupplier(@PathVariable Long id) { 
		Optional<Fournisseur> f = fournisseurService.findById(id);
		if(f.isPresent()) return ResponseEntity.ok().body(f.get());
		else return ResponseEntity.noContent().build();	
	}
	
	@DeleteMapping("/removeSupplier/{id}")
	public ResponseEntity<?> exampleRemoveSupplier(@PathVariable Long id) { 
		fournisseurService.remove(id);
		return ResponseEntity.ok().body(null);		
	}
	
}
