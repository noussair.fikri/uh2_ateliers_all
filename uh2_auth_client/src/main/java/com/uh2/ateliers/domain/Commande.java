package com.uh2.ateliers.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
//@Table(name="COMMANDE")
public class Commande {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="fournisseur_id")
	@JsonIgnore
	private Fournisseur fournisseur;
	
	private String reference;
	
	@JsonFormat(pattern="dd-MM-yyyy")
	private Date dateExpiration;
	
	private String designation;
	
	private String quantite;

	public Commande() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Commande(Long id, Fournisseur fournisseur, String reference, Date dateExpiration, String designation,
			String quantite) {
		super();
		this.id = id;
		this.fournisseur = fournisseur;
		this.reference = reference;
		this.dateExpiration = dateExpiration;
		this.designation = designation;
		this.quantite = quantite;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Date getDateExpiration() {
		return dateExpiration;
	}

	public void setDateExpiration(Date dateExpiration) {
		this.dateExpiration = dateExpiration;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getQuantite() {
		return quantite;
	}

	public void setQuantite(String quantite) {
		this.quantite = quantite;
	}

}
