package com.uh2.ateliers.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uh2.ateliers.domain.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client,Long> {
	
//	Client findById(String nomClient);

	Client findByNomClient(String nomClient);
}
