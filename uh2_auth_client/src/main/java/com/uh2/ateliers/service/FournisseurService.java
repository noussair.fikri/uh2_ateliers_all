package com.uh2.ateliers.service;

import java.util.List;
import java.util.Optional;

import com.uh2.ateliers.domain.Fournisseur;
import com.uh2.ateliers.dto.FournisseurDto;

public interface FournisseurService {

	Fournisseur save(FournisseurDto fournisseurDto);
	
	Fournisseur update(FournisseurDto fournisseurDto);
	
	List<Fournisseur> findAll();
	
	void remove(long id);
	
	Optional<List<Fournisseur>> findByNomFournisseur(String nomFournisseur);
	
	Optional<Fournisseur> findById(long id);
	
}
