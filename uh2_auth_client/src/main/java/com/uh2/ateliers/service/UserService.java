package com.uh2.ateliers.service;


import com.uh2.ateliers.domain.User;
import com.uh2.ateliers.dto.UserDto;

public interface UserService {
	
	User findByUsername(String username);
	
	User save(UserDto registration);

}
