package com.uh2.ateliers.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uh2.ateliers.domain.Role;
import com.uh2.ateliers.domain.User;
import com.uh2.ateliers.dto.UserDto;
import com.uh2.ateliers.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;

	
	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}
	
	public User save(UserDto registration) {
		
		User user =  new User();
		user.setNom(registration.getNom());
		user.setPrenom(registration.getPrenom());
		user.setUsername(registration.getUsername());
		user.setPassword(registration.getPassword());
		user.setRoles(Arrays.asList(
				new Role("ROLE_ADMIN"))
		);
		
		return userRepository.save(user);
	}
	
}
