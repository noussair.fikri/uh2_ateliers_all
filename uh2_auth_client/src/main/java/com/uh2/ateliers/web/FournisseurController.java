package com.uh2.ateliers.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uh2.ateliers.domain.Fournisseur;
import com.uh2.ateliers.domain.User;
import com.uh2.ateliers.dto.FournisseurDto;
import com.uh2.ateliers.dto.FournisseurDto;
import com.uh2.ateliers.dto.UserDto;
import com.uh2.ateliers.service.FournisseurService;
import com.uh2.ateliers.service.FournisseurService;
import com.uh2.ateliers.service.UserService;

@Controller
public class FournisseurController {
	
	@Autowired
	private FournisseurService fournisseurService;
	
	@GetMapping("/fournisseur")
	public String fournisseur(Model model)  {
		FournisseurDto newFournisseur = new FournisseurDto();
		model.addAttribute("new_fournisseur", newFournisseur);
		model.addAttribute("listFournisseurs", fournisseurService.findAll());
		return "fournisseur";
	}
	
	@GetMapping("/fournisseur/{action}/{id}")
	public String fournisseur(Model model, @PathVariable String action, @PathVariable long id)  {
		FournisseurDto newFournisseur = new FournisseurDto();
		model.addAttribute("new_fournisseur", newFournisseur);
		model.addAttribute("listFournisseurs", fournisseurService.findAll());
		if(action.equals("view"))
			model.addAttribute("currentFournisseur", fournisseurService.findById(id).get());
		else
			model.addAttribute("currentFournisseurEdit", fournisseurService.findById(id).get());
		return "fournisseur";
	}
	
	@PostMapping("/fournisseur/add")
	public String fournisseur_new(Model model, @ModelAttribute("new_fournisseur") @Valid FournisseurDto fournisseurDto, BindingResult result)  {
		
		if(fournisseurService.findByNomFournisseur(fournisseurDto.getNomFournisseur()).isPresent()) {
			result.rejectValue("nomFournisseur", null, "Ce fournisseur existe déjà sur la base");
			model.addAttribute("listFournisseurs",fournisseurService.findAll());
		}
		
		if(result.hasErrors()) {
			return "fournisseur";
		}
		
		fournisseurService.save(fournisseurDto);
		return "redirect:/fournisseur/?success";
		
	}
	
	@PostMapping("/fournisseur/update")
	public String fournisseur_update(Model model, @ModelAttribute("currentFournisseurEdit") @Valid FournisseurDto fournisseurDto, BindingResult result)  {
		
		if(fournisseurService.findByNomFournisseur(fournisseurDto.getNomFournisseur()).isPresent()) {
			result.rejectValue("nomFournisseur", null, "Ce fournisseur existe déjà sur la base");
			model.addAttribute("listFournisseurs",fournisseurService.findAll());
			model.addAttribute("currentFournisseur", fournisseurService.findById(fournisseurDto.getId()).get());
		}
		
		if(result.hasErrors()) {
			return "fournisseur";
		}
		
		fournisseurService.update(fournisseurDto);
		return "redirect:/fournisseur/view/"+fournisseurDto.getId()+"?successUpdate";
		
	}
	
	@GetMapping("/fournisseur/delete/{id}")
	public String fournisseur(Model model, @PathVariable long id)  {
		fournisseurService.remove(id);
		FournisseurDto newFournisseur = new FournisseurDto();
		model.addAttribute("new_fournisseur", newFournisseur);
		model.addAttribute("listFournisseurs", fournisseurService.findAll());

		return "fournisseur";
	}
	
}
