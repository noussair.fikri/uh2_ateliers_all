package com.aukhtubut.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Autowired
    private AuthenticationManager authenticationManager;
	
	@Override
    public void configure(final AuthorizationServerEndpointsConfigurer endpoints) {
		
		Map<String, CorsConfiguration> corsConfigMap = new HashMap<>();
	    CorsConfiguration config = new CorsConfiguration();
	    config.setAllowCredentials(true);
	    //TODO: Make configurable
	    config.setAllowedOrigins(Collections.singletonList("*"));
	    config.setAllowedMethods(Collections.singletonList("*")); // GET POST PUT DELETE OPTIONS
	    config.addAllowedHeader("access-control-allow-origin");
	    config.setAllowedHeaders(Collections.singletonList("*")); // "Authorization" : "basic 512d4104-10c2-4fa4-a044-fdf6fe7e1281"
	    corsConfigMap.put("/oauth/token", config);
	    corsConfigMap.put("/data/listData", config);
	    endpoints.getFrameworkEndpointHandlerMapping()
	            .setCorsConfigurations(corsConfigMap);
	    
		endpoints.tokenStore(tokenStore())
			.authenticationManager(authenticationManager);
	    
    }
	
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception 
      {
        security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.applyPermitDefaultValues();

        // add allow-origin to the headers
        config.addAllowedHeader("access-control-allow-origin");

        source.registerCorsConfiguration("/oauth/token", config);
        CorsFilter filter = new CorsFilter(source);
        security.addTokenEndpointAuthenticationFilter(filter);

    }


    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory() 
        .withClient("client") // client ID
        .secret("clientpassword") // Client secret
        .scopes("read", "write") 
        .authorizedGrantTypes("password")
        .accessTokenValiditySeconds(3600);
    }
    
	@Bean
	public TokenStore tokenStore() {
		return new InMemoryTokenStore();
	}
	
}