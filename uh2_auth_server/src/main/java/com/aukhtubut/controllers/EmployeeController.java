package com.aukhtubut.controllers;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aukhtubut.model.Employee;
@RestController
@RequestMapping("/admin")
public class EmployeeController {
	
	@GetMapping( "/granted")
    public ResponseEntity<String> granted() {
		HttpHeaders headers = new HttpHeaders();
	    headers.add(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");

	    return ResponseEntity.ok()
	            .headers(headers)
	            .body("Granted");
	}

	@GetMapping( "/getEmployeesList")
    public List<Employee> getEmployeesList() {
        List<Employee> employees = new ArrayList<>();
        Employee emp = new Employee();
        emp.setEmpId("emp1");
        emp.setEmpName("emp1");
        employees.add(emp);
        emp.setEmpId("emp2");
        emp.setEmpName("emp2");
        employees.add(emp);
        return employees;

    }

}