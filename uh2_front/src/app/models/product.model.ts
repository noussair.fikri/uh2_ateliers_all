export class Product {
  id?: any;
  image?: any;
  productName?: any;
  productPrice?: any;
  description?: any;
  available?: boolean;
}
